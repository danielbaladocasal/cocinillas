<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recipes';

    protected $fillable = [
        'season_id',
        'month_id',
        'name',
        'persons',
        'calories',
        'preparation_time_hours_total',
        'preparation_time_minutes_total',
        'preparation_time_hours_active',
        'preparation_time_minutes_active',
        'calories',
        'vegetarian',
        'vegan',
        'lactose_free',
        'gluten_free'
    ];

    public function images()
    {
        return $this->hasMany('App\RecipeImage');
    }
}
