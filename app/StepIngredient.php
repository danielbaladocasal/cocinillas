<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StepIngredient extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'step_ingredients';

    protected $fillable = [
        'step_id',
        'ingredient_id',
        'unit_id',
        'amount'
    ];
}
