<?php

namespace App;

class Season
{
    public static function all()
    {
        return json_decode(json_encode(config('constants.seasons')));
    }
}
