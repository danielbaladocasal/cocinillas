<?php namespace App;

class Helper
{
    public static function style($route)
    {
        return '<link media="all" type="text/css" rel="stylesheet" href="'.url($route).'?v='.filemtime(public_path($route)).'">';
    }

    public static function script($route)
    {
        return '<script src="'.url($route).'?v='.filemtime(public_path($route)).'"></script>';
    }

}