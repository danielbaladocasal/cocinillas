<?php

namespace App;


class Month
{
    public static function all()
    {
        return json_decode(json_encode(config('constants.months')));
    }
}
