<?php namespace App\Http\Controllers;

use App\Ingredient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IngredientController extends Controller {

    public function index()
    {
        dd('IngredientController index');
    }

    public function autoComplete(Request $request)
    {
        $input = $request->all();

        $suggestions = DB::table('ingredients')
            ->select('name as value', 'name as data')
            ->where('name', 'like', '%'.$input['query'].'%')
            ->take(6)
            ->get();

        $response = array(
            'query' => $input['query'],
            'suggestions' => $suggestions->toArray()
        );

        return $response;

    }

    public function addToForm()
    {
        return view('ingredient.form-input');
    }

}
