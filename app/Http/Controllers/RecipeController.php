<?php namespace App\Http\Controllers;

use App\Ingredient;
use App\Month;
use App\Recipe;
use App\RecipeStep;
use App\Season;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RecipeController extends Controller {

    public function index()
    {
        return \view('recipe.index');
    }

    public function create()
    {
        return \view('recipe.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $input['vegetarian'] = array_key_exists('vegetarian', $input) ? 1 : 0;
        $input['vegan'] = array_key_exists('vegetarian', $input) ? 1 : 0;
        $input['lactose_free'] = array_key_exists('vegetarian', $input) ? 1 : 0;
        $input['gluten_free'] = array_key_exists('vegetarian', $input) ? 1 : 0;

        $recipe = Recipe::create($input);

        return redirect()->route('recipe.complete', [$recipe->id]);
    }

    public function complete($id)
    {
        $recipe = Recipe::find($id);

        return \view('recipe.complete')->with([
            'recipe' => $recipe
        ]);
    }

    public function storeComplete($id, Request $request)
    {
        $input = $request->all();

        dd($input);

//        $recipe_step = RecipeStep::create([
//            'recipe_id' => $id
//        ]);

        foreach ($input['ingredient'] as $new_ingredient){
            $ingredient = Ingredient::where('name', trim($new_ingredient['name']))->first();

            if(!$ingredient){

                $ingredient = Ingredient::create(array(
                    'name' => $new_ingredient['name']
                ));
            }

        }

//        TODO bind ingredient to step
//        TODO add units to constants?

        return redirect()->route('recipe.complete', [$id]);
    }

    public function edit($id)
    {
        $recipe = Recipe::find($id);

        return \view('recipe.edit')->with([
            'recipe' => $recipe
        ]);
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {

    }

}
