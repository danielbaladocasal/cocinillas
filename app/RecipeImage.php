<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeImage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recipe_images';

    protected $fillable = [
        'recipe_id',
        'file'
    ];

    public function recipe()
    {
        return $this->belongsTo('App\Recipe');
    }
}
