$(document).ready(function() {

    $.ajaxSetup({headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')}});

    $('.autocomplete').autocomplete({
        serviceUrl: $('#updateForm').data('autocomplete-url')
    });

    $('body').on('click', '.add-ingredient', function(){

        // if($('.remove-file').length === 0) {
        //     $(this).parent().parent().parent().parent().remove();
        // }

        request = $.ajax({
            url: $('#ingredients-container').data('url'),
            type: 'GET',
            success: function(result) {
                $('#ingredients-container').append(result);
                $('.autocomplete').autocomplete({
                    serviceUrl: $('#updateForm').data('autocomplete-url')
                });
            }
        });

    });

    $('body').on('click', '.remove-ingredient', function(){

        // if($('.remove-file').length === 1) {
        //     $('#general').append('<div class="form-group" style="border-bottom: none;">\n' +
        //         '                        <div class="col-md-2 text-right">\n' +
        //         '                            <a class="btn btn-success add-file">+</a>\n' +
        //         '                        </div>\n' +
        //         '                    </div>');
        // }

        $(this).parent().parent().parent().parent().remove();

    });

});
