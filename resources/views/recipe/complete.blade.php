@extends('layout')

@section('meta')
    <meta name="description" content="Recetas">
    <meta name="title" content="Recetas">
    <title>Cocinillas | Editar receta</title>
@endsection

@section('css')
    @parent
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Editar receta</h1>

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link"  href="{{ route('recipe.edit', [$recipe->id]) }}">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('recipe.complete', [$recipe->id]) }}">Paso 1</a>
                    </li>
                </ul>

                <form id="updateForm" data-autocomplete-url="{{ route('ingredient.autocomplete') }}" action="{{ route('recipe.complete.store', [$recipe->id]) }}" method="post" enctype="multipart/form-data">

                    <div class="tab-content">

                        <div class="tab-pane active">

                            <div class="form-body">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="_method" value="post" />

                                <div id="ingredients-container" data-url="{{ route('ingredient.form.add') }}">

                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group">

                                                <label>Ingredientes</label>

                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <input type="text" name="ingredient[][name]" class="form-control autocomplete" placeholder="Nombre" />
                                                    </div>

                                                    <div class="col-md-3">
                                                        <input type="text" name="ingredient[][unit_id]" class="form-control" placeholder="Unidad" />
                                                    </div>

                                                    <div class="col-md-2">
                                                        <input type="text" name="ingredient[][amount]" class="form-control" placeholder="Cantidad" />
                                                    </div>

                                                    <div class="col-md-2">
                                                        <a class="btn btn-success add-ingredient" href="#">+</a>
                                                        <a class="btn btn-danger remove-ingredient" href="#">-</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <button type="submit" class="btn btn-primary">Guardar</button>

                            </div>

                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>


@endsection

@section('js')
    @parent
    {!! \App\Helper::script('js/jquery.autocomplete.min.js') !!}
    {!! \App\Helper::script('js/recipe/complete.js') !!}
@endsection
