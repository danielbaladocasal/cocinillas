@extends('layout')

@section('meta')
    <meta name="description" content="Recetas">
    <meta name="title" content="Recetas">
    <title>Cocinillas | Nueva receta</title>
@endsection

@section('css')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Nueva receta</h1>

                <form action="{{  route('recipe.store') }}" method="post">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="_method" value="post" />

                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="name">Nombre</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="persons">N. de personas</label>
                            <input type="text" class="form-control" id="persons" name="persons">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="calories">Calorias por persona</label>
                            <input type="text" class="form-control" id="calories" name="calories">
                        </div>
                    </div>
                    <label>Tiempo Preparado Total</label>
                    <div class="form-row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="preparation_time_hours_total">Horas</label>
                                <select class="form-control" id="preparation_time_hours_total" name="preparation_time_hours_total" >
                                    @for($i = 0; $i <= 10; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="preparation_time_minutes_total">Minutos</label>
                                <select class="form-control" id="preparation_time_minutes_total" name="preparation_time_minutes_total">
                                    @for($i = 0; $i < 60; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>

                    <label>Tiempo activo</label>
                    <div class="form-row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="preparation_time_hours_active">Horas</label>
                                <select class="form-control" id="preparation_time_hours_active" name="preparation_time_hours_active" >
                                    @for($i = 0; $i <= 10; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="preparation_time_minutes_active">Minutos</label>
                                <select class="form-control" id="preparation_time_minutes_active" name="preparation_time_minutes_active">
                                    @for($i = 0; $i < 60; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="vegetarian" name="vegetarian">
                                <label class="form-check-label" for="vegetarian">
                                    Vegetariana
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="vegan" name="vegan">
                                <label class="form-check-label" for="vegan">
                                    Vegana
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="lactose_free" name="lactose_free">
                                <label class="form-check-label" for="lactose_free">
                                    Sin lactosa
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="gluten_free" name="gluten_free">
                                <label class="form-check-label" for="gluten_free">
                                    Sin gluten
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="season_id">Estacion</label>
                            <select class="form-control" id="season_id" name="season_id">
                                @foreach(\App\Season::all() as $season)
                                    <option value="{{ $season->id }}">{{ $season->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="month_id">Estacion</label>
                            <select class="form-control" id="month_id" name="month_id">
                                @foreach(\App\Month::all() as $month)
                                    <option value="{{ $month->id }}">{{ $month->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Guardar</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
@endsection
