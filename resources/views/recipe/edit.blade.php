@extends('layout')

@section('meta')
    <meta name="description" content="Recetas">
    <meta name="title" content="Recetas">
    <title>Cocinillas | Editar receta</title>
@endsection

@section('css')
    @parent
@endsection

@section('content')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Editar receta</h1>

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('recipe.edit', [$recipe->id]) }}">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('recipe.complete', [$recipe->id]) }}">Pasos</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>

@endsection

@section('js')
    @parent
@endsection
