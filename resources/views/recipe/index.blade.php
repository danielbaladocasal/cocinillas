@extends('layout')

@section('meta')
    <meta name="description" content="Recetas">
    <meta name="title" content="Recetas">
    <title>Cocinillas | Recetas</title>
@endsection

@section('css')
    @parent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Lista de recetas</h1>
                <a class="btn btn-primary" href="{{ route('recipe.create') }}">Nueva</a>
                <br>
                <br>
                @foreach(\App\Recipe::all() as $recipe)
                    <a href="{{ route('recipe.edit', [$recipe->id]) }}">{{ $recipe->name }}</a><br>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
@endsection
