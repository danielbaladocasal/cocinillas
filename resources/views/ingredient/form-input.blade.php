<div class="form-row">
    <div class="col">
        <div class="form-group">

            <div class="row">
                <div class="col-md-5">
                    <input type="text" name="ingredient[][name]" class="form-control autocomplete" placeholder="Nombre" />
                </div>

                <div class="col-md-3">
                    <input type="text" name="ingredient[][unit_id]" class="form-control" placeholder="Unidad" />
                </div>

                <div class="col-md-2">
                    <input type="text" name="ingredient[][amount]" class="form-control" placeholder="Cantidad" />
                </div>

                <div class="col-md-2">
                    <a class="btn btn-success add-ingredient" href="#">+</a>
                    <a class="btn btn-danger remove-ingredient" href="#">-</a>
                </div>
            </div>
        </div>
    </div>

</div>
