<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect()->route('recipe.index');
});

Route::get('recipe/{id}/complete', [
    'uses' => 'RecipeController@complete',
    'as' => 'recipe.complete'
]);

Route::post('recipe/{id}/complete/store', [
    'uses' => 'RecipeController@storeComplete',
    'as' => 'recipe.complete.store'
]);

Route::get('ingredient/autocomplete', [
    'uses' => 'IngredientController@autoComplete',
    'as' => 'ingredient.autocomplete'
]);

Route::get('ingredient/add-to-form', [
    'uses' => 'IngredientController@addToForm',
    'as' => 'ingredient.form.add'
]);

Route::resource('recipe', 'RecipeController');
Route::resource('ingredient', 'IngredientController');
